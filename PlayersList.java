package com.example.r10;

import java.util.*;

public class PlayersList {
    ArrayList<Player> players = new ArrayList<Player>();

    public PlayersList() {
        R10MySQLConnection mySQLConnection = new R10MySQLConnection();
        Thread t = new Thread(mySQLConnection);
        try {
            t.start();
            t.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        players = mySQLConnection.getPlayers();
    }

    public ArrayList<Player> getPlayers(){
        return players;
    }


}
