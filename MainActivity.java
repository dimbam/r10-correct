package com.example.r10;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button button;
    private PlayersList Playerlist;
    private ArrayList<Integer> GWN;
    public static ArrayList<Integer> GWNArr;
    public static ArrayList<Player> SelectedPlayers;
    public static ArrayList<Player> SelectedPosition1Players;
    public static ArrayList<Player> SelectedPosition2Players;
    public static ArrayList<Player> SelectedPosition3Players;
    public static ArrayList<Player> SelectedPosition4Players;
    public static ArrayList<Player> SelectedPosition5Players;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        //R10MySQLConnection mySQLConnection = new R10MySQLConnection();
        Playerlist = new PlayersList();
        GWNArr = new ArrayList<>();
        SelectedPlayers = new ArrayList<>();
        SelectedPosition1Players = new ArrayList<>();
        SelectedPosition2Players = new ArrayList<>();
        SelectedPosition3Players = new ArrayList<>();
        SelectedPosition4Players = new ArrayList<>();
        SelectedPosition5Players = new ArrayList<>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int i = 0; i < Playerlist.players.size(); i++) {
            if(Playerlist.players.size()<1)
                GWNArr.add(Playerlist.players.get(i).getGame_week_number());
            else {
                if(GWNArr.contains(Playerlist.players.get(i).getGame_week_number()) == false) {
                    GWNArr.add(Playerlist.players.get(i).getGame_week_number());
                }
            }
        }

        for(int i=0; i<GWNArr.size(); i++){

            System.out.println("Players AARRRRRR++++++++++++++++++++++++++++++++++++++++" + GWNArr.get(i));
        }
        //System.out.println("444444444444444444444444444444444444444444444" + Playerlist.players.get(0).getPlayer_id());




        List<String> spinnerArray =  new ArrayList<String>();
        //GWN = mySQLConnection.getGWN();
        for(int i=0; i<GWNArr.size(); i++) {
            String item = Integer.toString(GWNArr.get(i));
            //String item = Integer.toString(Playerlist.players.get(i).getGame_week_number());

            spinnerArray.add(item);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner sItems = (Spinner) findViewById(R.id.gwnspinner);
        sItems.setAdapter(adapter);

        sItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                Toast.makeText(parent.getContext(),
                        "SelectedListener : " + parent.getItemAtPosition(pos).toString(),
                        Toast.LENGTH_SHORT).show();
                // An item was selected. You can retrieve the selected item using
                // parent.getItemAtPosition(pos)
                System.out.println(parent.getItemAtPosition(pos));
                for (int i = 0; i < Playerlist.players.size(); i++) {
                    String gwn = Integer.toString(Playerlist.players.get(i).getGame_week_number());

                    if (parent.getItemAtPosition(pos).toString().equals(gwn) == true) {
                        SelectedPlayers.add(Playerlist.players.get(i));
                    }

                }

                for (int i = 0; i < SelectedPlayers.size(); i++) {

                    System.out.println("Players Selected oooooooooododododododododododod----0000" + SelectedPlayers.get(i).getPlayer_id() + "positionnnnnnn" +SelectedPlayers.get(i).getPosition());

                }

                //Ελεγχος με βάση την θέση position==1 και δημιουργια λιστας παικτων μονο με θέση 1
                for (int i = 0; i < SelectedPlayers.size(); i++) {
                    if (SelectedPlayers.get(i).getPosition() == 1) {
                        SelectedPosition1Players.add(SelectedPlayers.get(i));

                    }
                }
                SelectedPlayersList(SelectedPosition2Players, 2);
                SelectedPlayersList(SelectedPosition3Players, 3);
                SelectedPlayersList(SelectedPosition4Players, 4);
                SelectedPlayersList(SelectedPosition5Players, 5);




                //Συμπληρωση των TextView
                TextView txtViewpos1 = (TextView) findViewById(R.id.pos1id);
                TextView txtViewpos2 = (TextView) findViewById(R.id.pos2id);
                TextView txtViewpos3 = (TextView) findViewById(R.id.pos3id);
                TextView txtViewpos4 = (TextView) findViewById(R.id.pos4id);
                TextView txtViewpos5 = (TextView) findViewById(R.id.pos5id);

                /*for(int i=0; i<SelectedPosition1Players.size(); i++){
                    float max = 0;
                    if(SelectedPosition1Players.get(i).getTendex()>max)
                        SelectedPosition1Players.get(i).getTendex() = max;
                }*/

                //καλω συναρτηση που βρισκει μεσω δεικτη tendex τον καλυτερο παικτη και επιστρεφει παικτη

                if (SelectedPosition1Players.size() > 0){
                    String temp_id = Integer.toString(FindBestPlayer(SelectedPosition1Players).getPlayer_id());
                    txtViewpos1.setText(temp_id);
                }
                if(SelectedPosition2Players.size()>0) {
                    String temp_id = Integer.toString(SelectedPosition2Players.get(0).getPlayer_id());
                    txtViewpos2.setText(temp_id);
                }
                if(SelectedPosition3Players.size()>0) {
                    String temp_id = Integer.toString(SelectedPosition3Players.get(0).getPlayer_id());
                    txtViewpos3.setText(temp_id);
                }
                if(SelectedPosition4Players.size()>0) {
                    String temp_id = Integer.toString(SelectedPosition4Players.get(0).getPlayer_id());
                    txtViewpos4.setText(temp_id);
                }
                if(SelectedPosition5Players.size()>0) {
                    String temp_id = Integer.toString(SelectedPosition5Players.get(0).getPlayer_id());
                    txtViewpos5.setText(temp_id);
                }

                SelectedPlayers.clear();
                SelectedPosition1Players.clear();
                SelectedPosition2Players.clear();
                SelectedPosition3Players.clear();
                SelectedPosition4Players.clear();
                SelectedPosition5Players.clear();




            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Another interface callback
            }
        });


    }
//Υπολογισμος λιστας με τους παικτες της αγωνιστικης που εχουν ως θεση την θεση του ορισματος position_number
    public void SelectedPlayersList(ArrayList <Player> PositionPlayersList,int position_number) {
        for(int i=0; i<SelectedPlayers.size(); i++){
            if(SelectedPlayers.get(i).getPosition()==position_number){
                PositionPlayersList.add(SelectedPlayers.get(i));

            }
        }
    }


    public Player FindBestPlayer(ArrayList <Player> PositionPlayersList){
        int best_player_index = 0;
        float max = 0;
        for(int i=0; i<PositionPlayersList.size(); i++){
            if(PositionPlayersList.get(i).getTendex()>max) {
                max = PositionPlayersList.get(i).getTendex();
                best_player_index = i;
            }
        }
        return PositionPlayersList.get(best_player_index);
    }


}